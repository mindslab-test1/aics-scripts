#!/bin/bash

#sudo check 
if [ "$(id -u)" != "0" ]; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

#Set server timezone 
sudo ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime

#yum update
sudo yum update -y

#install wget
sudo yum install -y  wget

#install curl
sudo yum install -y curl

#install docker
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io

#auto start docker(when server boot)
sudo systemctl start docker
sudo systemctl enable docker

#install docker-compose
sudo yum install -y jq
VERSION=$(curl --silent https://api.github.com/repos/docker/compose/releases/latest | jq .name -r)
sudo curl -L "https://github.com/docker/compose/releases/download/${VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
#Infra 팀에서 부터 부여받은 midns 계정에 docker그룹 권한
sudo gpasswd -a minds docker
newgrp docker


#install nginx
sudo echo "[nginx]
name=nginx repo
baseurl=https://nginx.org/packages/mainline/centos/7/\$basearch/
gpgcheck=0
enabled=1" >> /etc/yum.repos.d/nginx.repo
sudo yum update -y
sudo yum install -y nginx

#SELinux nginx proxy server localhost permission denied
sudo /usr/sbin/setsebool -P httpd_can_network_connect true

#install firewalld
sudo yum install -y firewalld
sudo systemctl start firewalld
sudo systemctl enable firewalld

#install openjdk8 
sudo yum install -y java-1.8.0-openjdk-devel.x86_64 &&
sudo cat > /etc/profile.d/java8.sh << EOF
export JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::")
export PATH=\$PATH:\$JAVA_HOME/bin
export CLASSPATH=.:\$JAVA_HOME/jre/lib:\$JAVA_HOME/lib:\$JAVA_HOME/lib/tools.jar
EOF

#install openjdk11
sudo yum install -y java-11-openjdk java-11-openjdk-devel

#install git
sudo yum install -y git

#install htop
sudo yum install -y epel-release
sudo yum install -y htop

#install vim
sudo yum install -y vim-enhanced

#set JAVA_HOME
source /etc/profile.d/java8.sh

#set public IP
sudo firewall-cmd --permanent --zone=public --add-source=125.132.250.204
