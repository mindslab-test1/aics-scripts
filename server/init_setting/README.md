# Server Setting Script


### 무엇인가?
 >mindslab에 적합한 서버 세팅 스크립트 입니다.
-  Centos 7 OS만 설치된 서버가 발급된 직후 초기세팅 스크립트

### 왜?
- 새로운 서버가 발급되거나, 기존에 있던 서버의 용도가 바뀜으로서 초기화 되었을 때

    개발 세팅을 하는데 반복적인 일을 수행하고 시간적 낭비가 발생하기 때문

### 어떻게?
>사용법
- SUDO 권한을 얻은 후 실행해주시기 바랍니다. 
```shell script
$ sudo su
````
- 실행 명령어 
```shell script 
$ ./server_setting.sh