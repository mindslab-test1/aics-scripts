# docker-compose-mongo.yml

```text
[Version]
docker: 
 - client-engine: 19.03.13
 - server-engine: 19.03.13
docker-compose: 1.25.4 ~ * 
```

## Setting

### Security

> "$HOME/mongo/mongod.conf.orig:/etc/mongod.conf.orig" 참고하여 아래 파일 내용을 포함하여 만들어 주세요.

```text
# mongod.conf

# for documentation of all options, see:
#   http://docs.mongodb.org/manual/reference/configuration-options/

# Where and how to store data.
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
#  engine:
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1


# how the process runs
processManagement:
  timeZoneInfo: /usr/share/zoneinfo

#security:

#operationProfiling:

#replication:

#sharding:

## Enterprise-Only Options:

#auditLog:

#snmp:
```

## 사용법

### Run docker-compose

> Go to the location where the docker-compose-mongo.yml file is located.

```shell script
$ docker-compose -f docker-compose-mongo.yml up -d
```

### Check Run, Network

```shell script
$ sudo docker-compose -f docker-compose-mongo.yml ps
$ sudo docker network ls
```

- docker-compose에서는 별다른 network 설정이 없으면 실행한 위치의 폴더명을 기준으로 network를 생성한다.
(Ex> {폴더명}_default)

### Stop, Remove

```shell script
$ sudo docker-compose -f docker-compose-mongo.yml stop
$ sudo docker-compose -f docker-compose-mongo.yml rm
```

### Init Setting

[Init Setting URL](https://pms.maum.ai/confluence/pages/viewpage.action?pageId=27949885)
