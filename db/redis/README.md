# docker-compose-redis.yml

```text
[Version]
docker: 
 - client-engine: 19.03.13
 - server-engine: 19.03.13
docker-compose: 1.25.4 ~ * 
```

## 사용법

### Run docker-compose

> Go to the location where the docker-compose-redis.yml file is located.

```shell script
$ SYSTEMPASS=<system-password> docker-compose -f docker-compose-redis.yml up -d
```

- system-password: SHA3-512 Hash Value (msl1234~)

### Check Run, Network

```shell script
$ sudo docker-compose -f docker-compose-redis.yml ps
$ sudo docker network ls
```

- docker-compose에서는 별다른 network 설정이 없으면 실행한 위치의 폴더명을 기준으로 network를 생성한다.
(Ex> {폴더명}_default)

### Stop, Remove

```shell script
$ sudo docker-compose -f docker-compose-redis.yml stop
$ sudo docker-compose -f docker-compose-redis.yml rm
```

## 최초 실행 후, 초기 세팅

[초기 세팅](https://www.notion.so/meloning/Redis-Stream-fe44915d82c640c4a780f229b7896233)
