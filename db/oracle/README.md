# docker-compose-redis.yml

```text
[Version]
docker: 
 - client-engine: 20.10.1 (미확인)
 - server-engine: 20.10.1 (미확인)
docker-compose: 1.25.4 ~ * 
```

## 사용법

### Create Volume Directory

분명하진 않지만 디렉토리를 먼저 생성하지 않을 경우 제대로 사용이 안되는 현상을 확인

```
    volumes:
      - "$HOME/oracle-data:/u01/app/oracle"
```

상기에 해당하는 경로를 추가하거나 변경하여 Persistency 확보

### Run docker-compose

> Go to the location where the docker-compose-oracle.yml file is located.

```shell script
$ docker-compose -f docker-compose-oracle.yml up -d
```

### Check Run, Network

```shell script
$ sudo docker-compose -f docker-compose-oracle.yml ps
$ sudo docker network ls
```

- docker-compose에서는 별다른 network 설정이 없으면 실행한 위치의 폴더명을 기준으로 network를 생성한다.
(Ex> {폴더명}_default)

### Stop, Remove

```shell script
$ sudo docker-compose -f docker-compose-oracle.yml stop
$ sudo docker-compose -f docker-compose-oracle.yml rm
```

## 최초 실행 후, 초기 세팅

초기 system username / password
```
username: system
password: oracle
```

접속 후 하기 query를 통해 password 변경. (확인 필요)
```
ALTER USER system IDENTIFIED BY <new_password>;
```

또는 사용자 추가.
[Confluence][https://pms.maum.ai/confluence/pages/viewpage.action?spaceKey=AD&title=Oracle+DB]
