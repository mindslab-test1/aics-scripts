# docker-compose-kafka.yml

```text
[Version]
docker: 
 - client-engine: 19.03.13
 - server-engine: 19.03.13
docker-compose: 1.25.4 ~ * 
```

## 사용법

### Run docker-compose

> Go to the location where the docker-compose-kafka.yml file is located.

```shell script
$ IP="$(dig +short myip.opendns.com @resolver1.opendns.com)" docker-compose -f docker-compose-kafka.yml up -d
```

- IP: docker-compose file 내에 명시된 환경변수 값이다.
- `dig +short myip.opendns.com @resolver1.opendns.com` : Get public ip

### Check Run, Network

```shell script
$ sudo docker-compose -f docker-compose-kafka.yml ps
$ sudo docker network ls
```

- docker-compose에서는 별다른 network 설정이 없으면 실행한 위치의 폴더명을 기준으로 network를 생성한다.
(Ex> {폴더명}_default)

### Stop, Remove

```shell script
$ sudo docker-compose -f docker-compose-kafka.yml stop
$ sudo docker-compose -f docker-compose-kafka.yml rm
```

