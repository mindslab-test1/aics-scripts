# AICS-Script Repository

AI 서비스 개발 3팀 내에서 shell script, docker-compose, Dockerfile 등을 관리하는 저장소입니다.

> AICS 개발팀이 조정되면서 AI 서비스 개발팀으로 합쳐졌습니다.

## aics-scripts repo 관리 방법

1. 작업할 내용을 jira 이슈료 만든 후, 생성된 이슈 코드 번호를 브랜치 명으로 생성 
2. 관리하고자 하는 파일의 용도에 맞게 폴더를 생성
3. 해당 스크립트에 대한 description 작성
   - 구성
   - 사용법
   - etc
4. commit 및 push한 후, pull request 요청
   > pull request 없이는 canary 브랜치에 merge할 수 없습니다.                               
   > pull request는 반드시 1명 이상의 리뷰어가 approve를 해주어야 merge가 가능합니다.
   >
   > 위 사항은 bitbucket repo setting에서 직접 설정해두었습니다.

5. 리뷰어가 리뷰한 후, 승인(approve)하면 canary 브랜치에 merge

## 관리 멤버

- junsu.jang@mindslab.ai (장준수)
- mljws0212@mindslab.ai (정우성)
- taeyanglim@mindslab.ai (임태양)
