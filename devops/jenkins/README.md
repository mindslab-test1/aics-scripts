# Jenkins Docker Container

## How to run jenkins container
```shell script
$ JENKINS_OPTS="/jenkins" USER=$(id -u) docker-compose -f docker-compose-jenkins.yml up -d
```

## Check Nginx conf

- IP: `114.108.173.106`
- USER: `ci`

```text
# /etc/nginx/conf.d/default.conf

...

# Devops - jenkinsci
location /jenkins {
    proxy_pass_header Authorization;
    proxy_pass http://127.0.0.1:9100;
    proxy_set_header Connection ~@~\~@~];

    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;

    # Required for new HTTP-based CLI
    proxy_http_version 1.1;
    proxy_request_buffering off;
    proxy_buffering off; # Required for HTTP-based CLI to work over SSL
}

...
```

> 멀티 도메인 활용 목적으로 aics-jenkins.conf에서 관리하였으나 어떤 요청이던 
> nginx default인 index.html 파일이 호출되는 이슈가 있었기에 default에서 일단 관리하기로 하였습니다.
>
> [Reference Link](https://serverfault.com/a/966750)

## jenkins_container_script.sh
- 젠킨스 컨테이너를 적재(up), 또는 제거(down) 할 수 있는 스크립트
- 젠킨스를 통해 분산 빌드환경 구성을 통해 발생하였던 restart시 slave 인식 불가 해결을 위한 후속 작업 추가
    - [[D550-192][Devops] Master-Agent 분산 구성 후 재기동시 인식이 되지 않는 이슈](https://pms.maum.ai/jira/browse/D550-192)

### command 1) docker-compose up -d
- 현재 호스트 머신의 jenkins container가 running status인지 확인
- 호스트 머신의 jenkins가 running이 아니라면 jenkins container 적재
- jenkins container를 적재한 뒤, container가 already 상태가 될때까지 health check
- jenkins container가 already라면 컨테이너의 **known_hosts** 파일 제거
- 해당 파일을 제거한 후, **ssh-copy-id**를 통해 slave01 컨테이너에 pub키 등록 및 known_hosts 생성
- 이미 running 상태라면 문자열 출력

### command 2) docker-compose down
- 현재 호스트 머신의 jenkins container가 running status인지 확인
- 해당 컨테이너가 running 상태라면 컨테이너 제거
- 이미 제거된 상태라면 문자열 출력
