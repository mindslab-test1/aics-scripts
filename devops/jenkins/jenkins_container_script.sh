#!/bin/bash

cmd_list=(" 1)up" " 2)down") # 커맨드 리스트(up = 적재, down = 제거)

echo -e "\033[1;31m select command \033[0m"

for str in ${cmd_list[@]}; do
	printf "\e[32m$str\e[m\n"
done

echo -ne "\033[33;1m >> execute command : \033[0m"
read select

jenkins_health_check() {
	local param_command=$1
	response_status_code=$(curl -s -o /dev/null -w "%{http_code}" "https://aics-stg.maum.ai/jenkins/login") # jenkins health check (200 = running)


	if [ $param_command -eq "1" ]; # up = 적재일 경우
	then
		if [ $response_status_code -eq "200" ];
		then
			echo -e "\033[31m"Jenkins is already running !!!"\033[0m"
			exit
		else
			echo "docker-compose up jenkins..."
			JENKINS_PREFIX="/jenkins" USER=$(id -u)  docker-compose -f ../docker-compose-jenkins.yml up -d # docker-compose up jenkins
			for (( ;; ))
			do
				jenkins_ready=$(curl -s -o /dev/null -w "%{http_code}" "https://aics-stg.maum.ai/jenkins/login")
				echo "wait for jenkins running..."
				if [ $jenkins_ready -eq "200" ];
				then
					break
				fi
				sleep 1
			done
			docker exec -it jenkins rm -rf /var/jenkins_home/.ssh/known_hosts
			docker exec -it jenkins ssh-copy-id -i /var/jenkins_home/.ssh/id_rsa.pub -o StrictHostKeyChecking=no slave01
			echo "jenkins is ready !!!"
		fi
	else
		if [ $response_status_code -eq "200" ];
		then
			echo "jenkins shutdown"
			docker-compose -f ../docker-compose-jenkins.yml down
			echo "\033[33;1m complete \033[0m"
		else
			echo -e "\033[31m"Jenkins is already stopping !!!"\033[0m"
			exit
		fi
	fi
}

jenkins_health_check $select