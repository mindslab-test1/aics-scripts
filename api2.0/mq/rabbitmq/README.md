# docker-compose-redis.yml

```text
[Version]
docker: 
 - client-engine: 20.10.1 (미확인)
 - server-engine: 20.10.1 (미확인)
docker-compose: 1.25.4 ~ * 
```

## 사용법

### Run docker-compose

> Go to the location where the docker-compose-redis.yml file is located.

```shell script
$ docker-compose -f docker-compose-rabbitmq.yml up -d
```

### Check Run, Network

```shell script
$ sudo docker-compose -f docker-compose-rabbitmq.yml ps
$ sudo docker network ls
```

- docker-compose에서는 별다른 network 설정이 없으면 실행한 위치의 폴더명을 기준으로 network를 생성한다.
(Ex> {폴더명}_default)

### Stop, Remove

```shell script
$ sudo docker-compose -f docker-compose-rabbitmq.yml stop
$ sudo docker-compose -f docker-compose-rabbitmq.yml rm
```
### rabbitmq.conf

디렉토리 내부에 존재하는 rabbitmq.conf에 기초적인 설정들이 존재한다.
```
default_user = minds
default_pass = msl1234~
```
이 부분에 최초 사용자 계정이 정의가 되어 있다. 그 외에 많은 부분들이 설정으로 조작할 수 있다. [참조](https://www.rabbitmq.com/configure.html).
```
loopback_users.guest = true
listeners.tcp.default = 5672
management.tcp.port = 15672
```
딱 봐도 알 수 있다시피 port 설정인데, 현재 compose의 port 설정은 rabbitmq default port가 붙어있기 때문에 이걸 바꾸면 사용이 애매해질것을 알아둘것.